//
//  AudioDelegate.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 22/03/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

class AudioDelegate : NSObject, AVAudioRecorderDelegate, AVAudioPlayerDelegate, SFSpeechRecognizerDelegate
{
    var vc : MainViewController
    
    init(vc: MainViewController)
    {
        self.vc = vc
    }
}
