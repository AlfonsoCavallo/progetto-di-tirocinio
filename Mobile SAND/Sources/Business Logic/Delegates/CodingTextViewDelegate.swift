//
//  CodingTextViewDelegate.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 18/04/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class CodingTextViewDelegate: NSObject, UITextViewDelegate
{
    let vc: MainViewController
    
    init(vc: MainViewController)
    {
        self.vc = vc
    }
    
    //Rimuove le intestazioni quando inizia a modificare
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = vc.codingTextView!.text
    }

    //Aggiunge le intestazioni quando finisce di modificare e si assicura che il cursore sia collocato su una riga esistente
    func textViewDidEndEditing(_ textView: UITextView) {
        let codingTextView = vc.codingTextView!
        codingTextView.text = textView.text
        codingTextView.adjustCursor()
    }
}
