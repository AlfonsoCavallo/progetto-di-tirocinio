//
//  CompilerInterface.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 08/05/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class CompilerInterface: NSObject
{
    //STRUTTURE PER PRENDERE I VALORI
    private struct CompilerSessionInformations: Decodable
    {
        var id: String //L'ID della sessione di compilazione per cui fare richiesta
        var status: String //Lo stato della richiesta
    }

    private struct CodeRequestStatusInformations: Decodable
    {
        var status: String //Status dell'operazione
    }
    
    private struct CodeBuildingInformations: Decodable
    {
        var build_result : String //Risultato della build
    }
    
    private struct CodeBadBuildingInformations: Decodable
    {
        var build_stderr : String //Risultato della build
    }
    
    private struct CodeExecutionInformations: Decodable
    {
        var stdout: String //Contenuto stream di output
    }
    
    private struct CodeBadExecutionInformations: Decodable
    {
        var stderr: String //Contenuto stream di errore
    }
    
    private struct CodeExecutionResultInformations: Decodable
    {
        var result: String //Risultato della richiesta
    }
    
    private var client : HTTPManager
    private let sessionRequestURL = "http://api.paiza.io/runners/create"
    private let executionRequestURL = "http://api.paiza.io/runners/get_details"
    
    override init()
    {
        client = HTTPManager()
    }
    
    //PARAMETRI VARI
    var parametersDictionary = ["source_code": "", "language": "swift", "api_key" : "guest"]
    
    func execute(codeFragment : String, in textView : StreamTextView)
    {
        //INVIO CODICE
        parametersDictionary["source_code"] = codeFragment
        client.makeAPICall(url: sessionRequestURL, params:parametersDictionary, method: .POST, success: { (data, response, error) in

            print(response!.statusCode)
            let id = try! JSONDecoder.init().decode(CompilerSessionInformations.self, from: data!).id
            sleep(2) //Attende un po' prima di richiedere il codice
            self.requestExecution(session: id, in: textView)
            
        }, failure: { (data, response, error) in
            DispatchQueue.main.async {
                textView.text = "ERROR: Cannot communicate with server"
            }
        })
    }
    
    private func requestExecution(session id: String, in stream : StreamTextView)
    {
        var streamOutput = ("", true) //L'output da restituire
        //RICEZIONE CODICE
        client.makeAPICall(url: executionRequestURL + "?id=" + id + "&api_key=guest", params: nil, method: .GET, success: { (data, response, error) in

            //Legge tutte le informazioni necessarie singolarmente
            let codeRequestStatusInformations = try? JSONDecoder.init().decode(CodeRequestStatusInformations.self, from: data ?? Data())
            let codeBuildingInformations = try? JSONDecoder.init().decode(CodeBuildingInformations.self, from: data ?? Data())
            let codeBadBuildingInformations = try? JSONDecoder.init().decode(CodeBadBuildingInformations.self, from: data ?? Data())
            let codeExecutionInformations = try? JSONDecoder.init().decode(CodeExecutionInformations.self, from: data ?? Data())
            let codeBadExecutionInformations = try? JSONDecoder.init().decode(CodeBadExecutionInformations.self, from: data ?? Data())
            let codeExecutionResultInformations = try? JSONDecoder.init().decode(CodeExecutionResultInformations.self, from: data ?? Data())
            
            //Stampa il risultato dell'esecuzione dando priorità al controllo di errori
            if codeRequestStatusInformations == nil || codeBuildingInformations == nil
            {
                streamOutput = ("ERRORE: No response from server", true)
            }
            else if codeRequestStatusInformations!.status == "running"
            {
                streamOutput = ("Still running", false)
            }
            else if codeBuildingInformations!.build_result == "failure" || codeBuildingInformations!.build_result == "error"
            {
                streamOutput = ("Build failed.\n" + codeBadBuildingInformations!.build_stderr, true)
            }
            else if codeExecutionResultInformations!.result == "failure" || codeExecutionResultInformations!.result == "error"
            {
                var output = ""
                if codeBadExecutionInformations != nil
                {
                    output.append("ERROR:\n" + codeBadExecutionInformations!.stderr + "\n")
                }
                streamOutput = (output, true)
            }
            else
            {
                var output = ""
                if (codeBadExecutionInformations?.stderr ?? "" ) != ""
                {
                    output.append("ERROR:\n" + codeBadExecutionInformations!.stderr + "\n")
                }
                if codeExecutionInformations != nil
                {
                    output.append("STDOUT:\n" + codeExecutionInformations!.stdout)
                }
                streamOutput = (output, true)
            }
            DispatchQueue.main.async {
                stream.text = streamOutput.0
            }

        }, failure: { (data, response, error) in
            streamOutput = ("Error during HTTP request at " + self.executionRequestURL, true)
        })
    }
}
