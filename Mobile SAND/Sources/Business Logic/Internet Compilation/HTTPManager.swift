//
//  HTTPManager.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 08/05/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class HTTPManager: NSObject
{
    
    //Metodi per le richieste HTTP
    enum HttpMethod : String {
       case  GET
       case  POST
       case  DELETE
       case  PUT
    }
    
    var request : URLRequest? //Richiesta HTTP
    var session : URLSession? //Sessione di comunicazione
        
    //Funzione per effettuare una chiamata HTTP
    //url: URL di destinazione della richiesta
    //params: Associazione chiave/valore dei file JSON inviati nel corpo della richiesta
    func makeAPICall(url: String,params: Dictionary<String, Any>?, method: HttpMethod, success:@escaping ( Data? ,HTTPURLResponse?  , NSError? ) -> Void, failure: @escaping ( Data? ,HTTPURLResponse?  , NSError? )-> Void) {
        //Inizializza una richiesta
        request = URLRequest(url: URL(string: url)!)
        //Se ci sono dei parametri prova ad inserirli nel corpo
        if let params = params {
            let  jsonData = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) //Li serializza come file JSON
            request?.setValue("application/json", forHTTPHeaderField: "Content-Type") //Dichiara il tipo di dati inviato
            request?.httpBody = jsonData //Inserisce i file nel corpo
        }
        request?.httpMethod = method.rawValue //Assegna il tipo di richiesta
        let configuration = URLSessionConfiguration.default //Configura la sessione di comunicazione

        configuration.timeoutIntervalForRequest = 30 //Imposta i timeouts
        configuration.timeoutIntervalForResource = 30 //Imposta i timeouts

        session = URLSession(configuration: configuration) //Inizializza la sessione

        //Assegna la richiesta alla sessione come task
        session?.dataTask(with: request! as URLRequest) { (data, response, error) -> Void in
            //Stabilisce se la richiesta è andata a buon fine
            if let data = data {
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    success(data , response , error as NSError?)
                } else {
                    failure(data , response as? HTTPURLResponse, error as NSError?)
                }
            }else {
                failure(data , response as? HTTPURLResponse, error as NSError?)
            }
        }.resume()
    }
}
