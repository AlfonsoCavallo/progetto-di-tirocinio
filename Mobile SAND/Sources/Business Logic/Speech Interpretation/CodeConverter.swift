//
//  TextToCodeConverter.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 22/03/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class CodeConverter: NSObject {
    
    private let dictionaryChecker : DictionaryChecker //Ricercatore nei dizionari
    typealias stms = DictionaryChecker.StatementsEnum
    typealias ntns = DictionaryChecker.NotationsEnum
    
    // MARK: Proprietà della conversione
    private var isTyping = false
    private var isChoosingNotation = false
    
    //Segnano dove inizia e finisce un comando
    private let startMarker = "░"
    private let endMarker = "▓"
    
    init(vc: MainViewController) throws
    {
        self.dictionaryChecker = vc.dictionaryChecker!
    }
    
    // MARK: Funzioni di conversione
    
    //Effettua la conversione finale
    func convert(_ string: String) -> (convertedString: String, closeAudioSession: Bool)
    {
        var words = stringToWords(introduceWhiteBetweenSpecialChar(string)) //Conserva la stringa come array
        //1. Converte i comandi di base nel linguaggio del sistema
        words = dictionaryChecker.convertTerms(words: words, in: dictionaryChecker.statementsDictionary)
        //2. Converte i comandi personalizzati nel linguaggio del sistema
        words = dictionaryChecker.convertTerms(words: words, in: dictionaryChecker.customsDictionary)
        //3. Converte i nomi delle notazioni
        words = dictionaryChecker.convertTerms(words: words, in: dictionaryChecker.notationsDictionary)
        //4. Esegue tutti i redo
        words = executeRedo(words)
        //5. Esegue tutti gli undo
        words = executeUndo(words)
        //6. Esegue tutte le clear
        words = executeClear(words)
        //7. Esegue tutte le type
        words = executeType(words)
        //8. Esegue tutte le literally
        words = specificExecution(on: words, with: executeLiterally)
        //9. Converte tutti i termini nelle corrispondenti keywords
        words = specificConversion(words: words, in: dictionaryChecker.keywordsDictionary)
        //10. Avvisa di un eventuale comando di interruzione alla fine e lo rimuove
        let closeResult = executeClose(words)
        words = closeResult.convertedWords
        let closeAudioSession = closeResult.closeAudioSession
        //11. Rimuove i marcatori dei comando
        words = removeMarkers(words)
        //12. Assembla la stringa
        let output = removeWhiteBetweenSpecialChar(wordsToString(words)) 
        return (convertedString: output, closeAudioSession: closeAudioSession)
    }
    
    //Esegue i "redo"
    private func executeRedo(_ words: [String]) -> [String]
    {
        var noMoreRedo = false
        let blockToExecute = [stms.undo.rawValue,stms.redo.rawValue]
        var convertedWords = words
        //Esegue il ciclo fino a che non è certo che non ci siano più redo da eseguire
        while !noMoreRedo
        {
            noMoreRedo = true
            var index = 0
            while index < convertedWords.count
            {
                //Rimuove i redo singoli
                if convertedWords[index] == stms.redo.rawValue
                {
                    convertedWords.remove(at: index)
                }
                //Se il blocco corrisponde lo elimina
                else if index + 1 < convertedWords.count && Array(convertedWords[index...(index + 1)]) == blockToExecute
                {
                    convertedWords.removeSubrange(index...(index + 1))
                    if index > 0
                    {
                        index -= 1
                    }
                    noMoreRedo = false
                }
                else
                {
                    index += 1
                }
            }
        }
        return convertedWords
    }
    
    //Esegue gli "undo"
    private func executeUndo(_ words: [String]) -> [String]
    {
        var noMoreUndo = false
        var convertedWords = words
        //Esegue il ciclo fino a che non è certo che non ci siano più undo da eseguire
        while !noMoreUndo
        {
            noMoreUndo = true
            var index = 0
            while index < convertedWords.count
            {
                //Rimuove il primo "undo" se c'è
                if index < convertedWords.count && convertedWords[index] == stms.undo.rawValue
                {
                    convertedWords.remove(at: index)
                    noMoreUndo = false
                }
                //Rimuove tutti gli "undo" preceduti da una parola che non sia "undo"
                else if index + 1 < convertedWords.count && convertedWords[index] != stms.undo.rawValue && convertedWords[index + 1] == stms.undo.rawValue
                {
                    convertedWords.removeSubrange(index...(index + 1))
                    if index > 0
                    {
                        index -= 1
                    }
                    noMoreUndo = false
                }
                else
                {
                    index += 1
                }
            }
        }
        
        return convertedWords
    }
    
    //Esegue le clear
    private func executeClear(_ words: [String]) -> [String]
    {
        var convertedWords = words
        var index = 0
        while index < convertedWords.count
        {
            //Se trova clear cancella tutto il precedente
            if convertedWords[index] == stms.clear.rawValue
            {
                var returnIndex = index
                var returnFound = false
                //Cerca il primo "\n" e cancella tutto quello che c'è dopo
                while returnIndex >= 0 && !returnFound
                {
                    if convertedWords[returnIndex] == "\n"
                    {
                        returnFound = true
                    }
                    else
                    {
                        returnIndex -= 1
                    }
                }
                convertedWords.removeSubrange((returnIndex + 1)...index)
                index = 0
            }
            else
            {
                index += 1
            }
        }
        return convertedWords
    }
    
    //Esegue le type
    private func executeType(_ words: [String]) -> [String]
    {
        var convertedWords = [String]()
        var index = 0
        var isTyping = false //Indica se si è in fase di scrittura
        var formattedWords = [String]()
        
        while index < words.count
        {
            if !isTyping && words[index] == stms.type.rawValue //Se non ha trovato una "type" la cerca, pulisce la variabile dove inserire le parole da formattare e attiva la flag isTyping
            {
                convertedWords.append(startMarker)
                formattedWords.removeAll()
                isTyping = true
            }
            else if isTyping && words[index] == stms._in.rawValue //Se trova una "in" formatta la stringa e la inserisce nella sequenza di output
            {
                isTyping = false
                if index + 1 < words.count
                {
                    index += 1
                    var unknownCase = false
                    let formattedWordsCount = formattedWords.count //Memorizza il numero di parole raccolto
                    switch words[index]
                    {
                    case ntns.camelCase.rawValue:
                        formattedWords = NotationFormatter.camelCase(formattedWords)
                    case ntns.pascalCase.rawValue:
                        formattedWords = NotationFormatter.pascalCase(formattedWords)
                    case ntns.snakeCase.rawValue:
                        formattedWords = NotationFormatter.snakeCase(formattedWords)
                    case ntns.kebabCase.rawValue:
                        formattedWords = NotationFormatter.kebabCase(formattedWords)
                    case ntns.upperCase.rawValue:
                        formattedWords = NotationFormatter.upperCase(formattedWords)
                    case ntns.titleCase.rawValue:
                        formattedWords = NotationFormatter.titleCase(formattedWords)
                    case ntns.stringCase.rawValue:
                        formattedWords = NotationFormatter.stringCase(formattedWords)
                    case ntns.spongeCase.rawValue:
                        formattedWords = NotationFormatter.spongeCase(formattedWords)
                    default:
                        unknownCase = true
                        break
                    }
                    if(formattedWords.count > 0)
                    {
                        convertedWords.removeSubrange((convertedWords.count - formattedWordsCount)...(convertedWords.count - 1))
                    }
                    convertedWords.append(contentsOf: formattedWords)
                    convertedWords.append(endMarker)
                    //Se il formato è sconosciuto lo scrive dopo l'endMarker per notificare il fraintendimento
                    if unknownCase
                    {
                        convertedWords.append(words[index])
                    }
                }
                else
                {
                    convertedWords.append(words[index])
                }
            }
            else if isTyping && (words[index] == stms.literally.rawValue || words[index] == stms.newLine.rawValue)  //Se trova literally inserisce il segnalino di fine comando
            {
                isTyping = false
                convertedWords.append(endMarker)
                if words[index] == stms.newLine.rawValue
                {
                    convertedWords.append("\n")
                }
            }
            else if isTyping //Altrimenti aggiunge la parola trovata alla sequenza
            {
                convertedWords.append(words[index])
                formattedWords.append(words[index])
            }
            else
            {
                convertedWords.append(words[index])
            }
            index += 1
        }
        return convertedWords
    }
    
    //Esegue le literally
    private func executeLiterally(_ words: [String]) -> [String]
    {
        var isNextLiteral = false
        var convertedWords = [String]()
        for elem in words
        {
            if elem == stms.literally.rawValue
            {
                convertedWords.append(startMarker)
                isNextLiteral = true
            }
            else if isNextLiteral
            {
                convertedWords.append(elem)
                convertedWords.append(endMarker)
                isNextLiteral = false
            }
            else
            {
                convertedWords.append(elem)
            }
        }
        return convertedWords
    }
    
    private func removeMarkers(_ words: [String]) -> [String]
    {
        let convertedWords = words.filter { $0 != startMarker && $0 != endMarker }
        return convertedWords
    }
    
    //Esegue la close
    private func executeClose(_ words: [String]) -> (convertedWords: [String], closeAudioSession: Bool)
    {
        if words.last == DictionaryChecker.StatementsEnum.stop.rawValue
        {
            var convertedWords = words
            convertedWords.removeLast()
            return (convertedWords: convertedWords, closeAudioSession: true)
        }
        return (convertedWords: words, closeAudioSession: false)
    }
    
    //Esegue la funzione execute solo sulle sottosequenze di parole che non sono comprese tra command marker
    private func specificExecution(on words: [String], with execute: ([String]) -> [String]) -> [String]
    {
        var convertedWords = [String]()
        var arrayToExecute = [String]()
        var isStartMarkerFound = false
        //Filtra tutti gli elementi compresi tra startMarker ed endMarker
        for elem in words
        {
            if elem == startMarker //Segnala che si sta entrando in un comando
            {
                isStartMarkerFound = true
                convertedWords.append(contentsOf: execute(arrayToExecute))
                arrayToExecute.removeAll()
                convertedWords.append(startMarker)
            }
            else if isStartMarkerFound && elem == endMarker //Segnala che si sta uscendo da un comando
            {
                isStartMarkerFound = false
                convertedWords.append(endMarker)
            }
            else if isStartMarkerFound //Se lo startMarker è trovato aggiunge direttamente gli elementi all'output
            {
                convertedWords.append(elem)
            }
            else //Altrimenti li aggiunge ad un array che deve essere convertito dalla funzione execute
            {
                arrayToExecute.append(elem)
            }
        }
        convertedWords.append(contentsOf: execute(arrayToExecute))
        return convertedWords
    }
    
    //Stessa cosa della funzione precedente ma per le conversione
    private func specificConversion(words: [String], in dictionary: DictionaryChecker.Dictionary) -> [String]
    {
        var convertedWords = [String]()
        var arrayToExecute = [String]()
        var isStartMarkerFound = false
        //Filtra tutti gli elementi compresi tra startMarker ed endMarker
        for elem in words
        {
            if elem == startMarker //Segnala che si sta entrando in un comando
            {
                isStartMarkerFound = true
                convertedWords.append(contentsOf: dictionaryChecker.convertTerms(words: arrayToExecute, in: dictionary))
                arrayToExecute.removeAll()
                convertedWords.append(startMarker)
            }
            else if isStartMarkerFound && elem == endMarker //Segnala che si sta uscendo da un comando
            {
                isStartMarkerFound = false
                convertedWords.append(endMarker)
            }
            else if isStartMarkerFound //Se lo startMarker è trovato aggiunge direttamente gli elementi all'output
            {
                convertedWords.append(elem)
            }
            else //Altrimenti li aggiunge ad un array che deve essere convertito dalla funzione execute
            {
                arrayToExecute.append(elem)
            }
        }
        convertedWords.append(contentsOf: dictionaryChecker.convertTerms(words: arrayToExecute, in: dictionary))
        return convertedWords
    }
}
