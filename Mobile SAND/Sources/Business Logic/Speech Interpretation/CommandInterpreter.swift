//
//  CommandInterpreter.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 25/04/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class CommandInterpreter: NSObject {
    
    private var dictionaryChecker : DictionaryChecker //Ricercatore nei dizionari
    private var executedCommands = 0 //Tiene conto di quanti comandi sono già stati eseguiti
    private var ignoredCommands = 0 //Comandi attualmente ignorati
    private var maxScriptNameLength = 15 //Calcolati in parole
    private var defaultFileNotation = NotationFormatter.snakeCase
    
    //References ad altre classi
    private var vc : MainViewController //Il reference al view controller
    private var codingTextView : CodingTextView //Shortcut per la codingTextView
    private var streamTextView : StreamTextView //Shortcut per la streamTextView
    
    //Array di comandi
    private var commandInputWords = [String]() //E' l'array di parole usato nelle operazioni
    
    init(vc: MainViewController) throws
    {
        self.dictionaryChecker = vc.dictionaryChecker!
        self.vc = vc
        self.codingTextView = vc.codingTextView!
        self.streamTextView = vc.streamTextView!
    }
    
    //Esegue tutti i comandi nella stringa. Restituisce:
    // - "CloseAudioSession" se c'è un comando di chiusura
    // - Una stringa da convertire in codice se c'è un comando di scrittura
    // - nil se è in attesa di altri comandi
    func execute(_ commandInput : String) -> String?
    {
        let words = commandInput.lowercased().components(separatedBy: " ")
        commandInputWords = dictionaryChecker.convertTerms(words: words, in: dictionaryChecker.commandsDictionary) //Converte tutti i termini e li riassegna all'array di parole
        
        ignoredCommands = 0 //Azzera i comandi attualmente ignorati
        var index = 0
        while index < commandInputWords.count
        {
            let command = isItACommand(commandInputWords[index]) //Verifica se la parola è un comando
            let closeAudioSessionToken = "CloseAudioSession"
            switch command
            {
            case .line:
                //Verifica che ci sia una parola dopo index e che sia un numero
                //SpeechFix: Utilizza la funzione convertToNumber per le parole non riconosciute dallo speechToText come numeri
                if index + 1 < commandInputWords.count && isInt(convertToNumber(commandInputWords[index + 1]))
                {
                    let lineNumber = Int(convertToNumber(commandInputWords[index + 1]))!
                    if lineNumber > 0 && lineNumber <= codingTextView.lines.count //Se il numero di riga corrisponde ad una riga esistente vi ci sposta il cursore
                    {
                        decideExecution{executeLine(lineNumber)}
                    }
                }
                break
            case .cut:
                decideExecution(of: executeCut)
            case .copy:
                decideExecution(of: executeCopy)
            case .paste:
                decideExecution(of: executePaste)
            case .stop:
                return closeAudioSessionToken
            case .read:
                decideExecution(of: executeRead)
                return closeAudioSessionToken
            case .write:
                if index + 1 < commandInputWords.count
                {
                    return executeWrite(at: index)
                }
            case .delete:
                decideExecution(of: executeDelete)
            case .run:
                decideExecution(of: executeRun)
                return closeAudioSessionToken
            case .undo:
                break //CodeFor: undo
            case .redo:
                break //CodeFor: redo
            case .new:
                executeNew()
                return closeAudioSessionToken
            case .save:
                executeSave()
                return closeAudioSessionToken
            case .open:
                if commandInputWords.count - index < maxScriptNameLength //Se il numero di parole non eccede
                {
                    var filename = [String]() //Nuovo nome per il file
                    vc.streamTextView!.text = "" //Pulisce la stream view
                    index += 1 //Incrementa l'indice
                    while index < commandInputWords.count
                    {
                        if(commandInputWords[index] == DictionaryChecker.CommandsEnum.stop.rawValue) //Se rileva uno stop
                        {
                            if filename.count > 0 { //Se non c'è un filename valido interrompe la sessione senza eseguire codice
                                let fileToOpen = defaultFileNotation(filename).first! + ".swift"
                                if(vc.documentsURL != nil)
                                {
                                    vc.open(vc.documentsURL!.appendingPathComponent(fileToOpen))
                                }
                            }
                            return closeAudioSessionToken
                        }
                        filename.append(commandInputWords[index])
                        index += 1
                    }
                    streamTextView.text = "File to open: " + defaultFileNotation(filename).first!
                }
                else
                {
                    streamTextView.text = "Max legnth for file name exceeded"
                    return closeAudioSessionToken
                }
                break
            case .rename:
                if commandInputWords.count - index < maxScriptNameLength //Se il numero di parole non eccede
                {
                    var filename = [String]() //Nuovo nome per il file
                    vc.streamTextView!.text = "" //Pulisce la stream view
                    index += 1 //Incrementa l'indice
                    while index < commandInputWords.count
                    {
                        if(commandInputWords[index] == DictionaryChecker.CommandsEnum.stop.rawValue) //Se rileva uno stop
                        {
                            if filename.count > 0 { //Se non c'è un filename valido interrompe la sessione
                                vc.scriptName = defaultFileNotation(filename).first!
                                vc.save()
                            }
                            return closeAudioSessionToken
                        }
                        filename.append(commandInputWords[index])
                        index += 1
                    }
                    streamTextView.text = "New file name: " + defaultFileNotation(filename).first!
                }
                else
                {
                    streamTextView.text = "Max legnth for file name exceeded"
                    return closeAudioSessionToken
                }
                break
            @unknown default:
                break
            }
        index += 1
        }
        return nil
    }
    
    // MARK: Comandi Eseguibili
    
    //Stabilisce se eseguire la funzione o meno in base a quanti comandi sono stati già ignorati
    private func decideExecution(of function : () -> ())
    {
        if ignoredCommands == executedCommands //Se i comandi ignorati pareggiano quelli già eseguiti esegue, altrimenti ignora
        {
            function()
            executedCommands += 1
        }
        else
        {
            ignoredCommands += 1
        }
    }
    
    //Converte to e for eventualmente in 2 e 4
    private func convertToNumber(_ string: String) -> String
    {
        if string == "to"
        {
            return "2"
        }
        if string == "for"
        {
            return "4"
        }
        return string
    }
    
    //Esegue il comando line
    private func executeLine(_ lineNumber : Int)
    {
        codingTextView.lineCursor = lineNumber
    }
    
    //Esegue il comando cut
    private func executeCut()
    {
        if codingTextView.lines.count > 0
        {
            UIPasteboard.general.string = codingTextView.lines.remove(at: codingTextView.lineCursor - 1)
            codingTextView.adjustCursor()
        }
    }
    
    //Esegue il comando copy
    private func executeCopy()
    {
        if codingTextView.lines.count > 0
        {
            UIPasteboard.general.string = codingTextView.lines[codingTextView.lineCursor - 1]
        }
    }
    
    //Esegue la funzione paste
    private func executePaste()
    {
        //Se c'è una stringa copiata
        if UIPasteboard.general.string != nil
        {
            codingTextView.lines.insert(contentsOf: textToLines(UIPasteboard.general.string!), at: codingTextView.lineCursor)
            codingTextView.lineCursor += 1
        }
    }
    
    //Esegue il comando read
    private func executeRead()
    {
        if codingTextView.lines.count > 0 && codingTextView.lineCursor != 0
        {
            syntetize(codingTextView.lines[codingTextView.lineCursor - 1]) //Legge la riga
        }
    }
    
    //Esegue il comando execute
    private func executeRun()
    {
        vc.run()
    }
    
    //Esegue il comando delete
    private func executeDelete()
    {
        if codingTextView.lines.count > 0
        {
            codingTextView.lines.remove(at: codingTextView.lineCursor - 1)
            codingTextView.adjustCursor()
        }
    }
    
    //Esegue il comando write (l'unico che non incrementa la executedCommand)
    private func executeWrite(at index: Int) -> String
    {
        //Restituisce come stringa tutto quello che c'è dopo
        return wordsToString([String](commandInputWords[(index + 1)...(commandInputWords.count - 1)])).replacingOccurrences(of: " \n ", with: "\n")
    }
    
    //Esegue il comando new
    private func executeNew()
    {
        vc.createNewScript() //Crea un nuovo script
    }
    
    //Esegue il comando save
    private func executeSave()
    {
        if vc.scriptName != nil { vc.save() } //Se il file ha un nome effettua il salvataggio
        else { streamTextView.text = "You should name the script first"} //Altrimenti suggerisce di rinominare il file
    }
    
    // MARK: Funzioni utili
    
    //Restituisce il comando corrispondente al parametro
    private func isItACommand(_ word : String) -> DictionaryChecker.CommandsEnum?
    {
        return DictionaryChecker.CommandsEnum.init(rawValue: word)
    }
}
