//
//  DictionaryChecker.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 22/03/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class DictionaryChecker: NSObject {
    
    typealias Dictionary = [String : [String]]
    
    // MARK: Enumerazioni
    private let longestExpressionInDictionary = 3 //La lunghezza dell'espressione più lunga
    
    //Tipi di dizionari
    enum DictionaryEnum
    {
        case keywords
        case statements
        case customs
        case notations
        case commands
        //Dizionari per informazioni
        case buttons
        case informations
        case commandsInfo
        case statementsInfo
    }
    
    //Tipi di comandi della IDE
    enum CommandsEnum : String
    {
        case line = "line"
        case read = "read"
        case run = "run"
        case cut = "cut"
        case copy = "copy"
        case paste = "paste"
        case delete = "delete"
        case write = "write"
        case from = "from"
        case to = "to"
        case all = "all"
        case stop = "stop"
        case undo = "undo"
        case redo = "redo"
        case new = "new"
        case save = "save"
        case open = "open"
        case rename = "rename"
    }
    
    //Tipi di info
    enum InfoEnum : String
    {
        case commands = "commands"
        case statements = "statements"
        case keywords = "keywords"
        case notations = "notations"
    }
    
    //Tipi di bottoni
    enum ButtonEnum: String
    {
        case organize = "Organize"
        case save = "Save"
        case add = "Add"
        case mic = "Mic"
        case run = "Run"
    }
    
    //Tipi di comandi di conversione
    enum StatementsEnum : String
    {
        case redo = "redo"
        case undo = "undo"
        case clear = "clear"
        case type = "type"
        case literally = "literally"
        case newLine = "\n"
        case _in = "in"
        case next = "next"
        case nextLine = "next line"
        case stop = "stop"
    }
    
    //Tipi di notazione
    enum NotationsEnum : String
    {
        case camelCase = "camelCase"
        case pascalCase = "pascalCase"
        case snakeCase = "snakeCase"
        case kebabCase = "kebabCase"
        case upperCase = "upperCase"
        case titleCase = "titleCase"
        case stringCase = "stringCase"
        case spongeCase = "spongeCase"
    }
    
    // MARK: Variabili dei files
    
    var buttons : Dictionary
    var informations : Dictionary
    var commandsInfo : Dictionary
    var statementsInfo : Dictionary
    
    //Dizionari per il convertitore
    var keywordsDictionary : Dictionary
    var statementsDictionary : Dictionary
    var customsDictionary : Dictionary
    var notationsDictionary : Dictionary
    var commandsDictionary : Dictionary
    static let defaultLang = "en"
    
    
    init(_ lang : String) throws
    {
        //Carica tutti i dizionari
        try self.keywordsDictionary = DictionaryChecker.dictionaryLoad(get: .keywords, with: lang)
        try self.statementsDictionary = DictionaryChecker.dictionaryLoad(get: .statements, with: lang)
        try self.customsDictionary = DictionaryChecker.dictionaryLoad(get: .customs, with: lang)
        try self.notationsDictionary = DictionaryChecker.dictionaryLoad(get: .notations, with: lang)
        try self.commandsDictionary = DictionaryChecker.dictionaryLoad(get: .commands, with: lang)
        //Carica i dizionari per le informazioni
        try self.buttons = DictionaryChecker.dictionaryLoad(get: .buttons, with: lang)
        try self.informations = DictionaryChecker.dictionaryLoad(get: .informations, with: lang)
        try self.commandsInfo = DictionaryChecker.dictionaryLoad(get: .commandsInfo, with: lang)
        try self.statementsInfo = DictionaryChecker.dictionaryLoad(get: .statementsInfo, with: lang)
    }
    
    
    //Carica un dizionario da file
    static private func dictionaryLoad(get type: DictionaryEnum, with lang: String) throws -> Dictionary
    {
        var dictionary = Dictionary()
        var dictionaryFileString : String
        var filename = String(lang.prefix(2))
        var defaultFilename = defaultLang
        
        switch type
        {
        case .keywords:
            filename.append("_keywords")
            defaultFilename.append("_keywords")
            break
        case .statements:
            filename.append("_statements")
            defaultFilename.append("_statements")
            break
        case .customs:
            filename.append("_customs")
            defaultFilename.append("_customs")
            break
        case .notations:
            filename.append("_notations")
            defaultFilename.append("_notations")
            break
        case .commands:
            filename.append("_commands")
            defaultFilename.append("_commands")
            //Informazioniu
        case .buttons:
            filename.append("_buttons")
            defaultFilename.append("_buttons")
        case .informations:
            filename.append("_informations")
            defaultFilename.append("_informations")
        case .commandsInfo:
            filename.append("_commands_info")
            defaultFilename.append("_commands_info")
        case .statementsInfo:
            filename.append("_statements_info")
            defaultFilename.append("_statements_info")
        }
        let path = Bundle.main.path(forResource: filename, ofType: "txt")
        let defaultFilePath = Bundle.main.path(forResource: defaultFilename, ofType: "txt")

        //Prova a caricare il contenuto del dizionario selezionato altrimenti quello inglese
        try dictionaryFileString = String(contentsOfFile: path ?? defaultFilePath ?? "")
        
        //Utilizza il carattere di ritorno per separare le righe
        var rows = dictionaryFileString.components(separatedBy: "\n")
        if rows.last! == ""
        {
            rows.removeLast()
        }
        
        //In ogni riga individua chiave e valori
        for row in rows
        {
            let pair = row.components(separatedBy: " - ")
            if(pair.count == 2)
            {
                let key = pair[0]
                let values = pair[1].components(separatedBy: ", ") //I valori sono separati dalla virgola
                dictionary[key] = values
            }
        }
        return dictionary
    }
    
    // MARK: Funzioni di ricerca
    //Converte una parola o un espressione nel carattere o operatore corrispondente
    private func convertWord(from array: [String], by index: Int, in dictionary: [String:[String]]) -> (str: String, offset: Int)?
    {        //Cerca la massima lunghezza dell'espressione nello spazio rimanente
        var longestExpressionInArray = longestExpressionInDictionary
        while index + longestExpressionInArray - 1 >= array.count && longestExpressionInArray > 0
        {
            longestExpressionInArray -= 1
        }
        //Se non rimane spazio restituisce nil
        if longestExpressionInArray == 0
        {
            return nil
        }
        //Altrimenti cerca nel dizionario partendo dall'espressione più lunga
        var length = longestExpressionInArray
        while length > 0
        {
            //Controlla ogni elemento del dizionario
            for elem in dictionary
            {
                //Controlla ogni espressione legata a quell'elemento
                for exprToConfront in elem.value
                {
                    let expr = array[index ..< index + length]
                    if expr.joined(separator: " ") == exprToConfront
                    {
                        return (str: elem.key, offset: length - 1)
                    }
                }
            }
            length -= 1
        }
        return nil
    }
    
    //Converte un array di parole o espressioni sostituendo i termini con il loro termine associato
    func convertTerms(words: [String], in dictionary: Dictionary) -> [String]
    {
        var i = 0
        var convertedWords = [String]()
        while i < words.count
        {
            let conversionOutput = convertWord(from: words, by: i, in: dictionary) ?? (str: words[i], offset: 0)
            convertedWords.append(conversionOutput.str)
            i += 1 + conversionOutput.offset
        }
        return convertedWords
    }
}
