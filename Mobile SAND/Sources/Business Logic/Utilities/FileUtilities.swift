//
//  FileUtilities.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 22/06/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import Foundation

//Apre il file all'URL selezionato
func fileOpen(_ URL: URL) throws -> String
{
    return try String(contentsOf: URL)
}

//Salva il file all'URL selezionato
func fileSave(URL: URL, content: String) throws
{
    try content.write(to: URL, atomically: true, encoding: String.Encoding.utf8)
}
