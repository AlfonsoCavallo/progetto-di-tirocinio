//
//  NotationFormatter.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 31/03/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class NotationFormatter
{
    static func camelCase(_ words: [String]) -> [String]
    {
        var convertedWords = words
        for index in 0..<convertedWords.count
        {
            print(index, convertedWords.count)
            if index > 0
            {
                convertedWords[index] = convertedWords[index].capitalized
            }
        }
        convertedWords = [wordsToString(convertedWords).replacingOccurrences(of: " ", with: "")]
        return convertedWords
    }
    
    static func pascalCase(_ words: [String]) -> [String]
    {
        var convertedWords = words
        for index in 0..<convertedWords.count
        {
            convertedWords[index] = convertedWords[index].capitalized
        }
        convertedWords = [wordsToString(convertedWords).replacingOccurrences(of: " ", with: "")]
        return convertedWords
    }
    
    static func snakeCase(_ words: [String]) -> [String]
    {
        var convertedWords = words
        convertedWords = [convertedWords.joined(separator: "_")]
        return convertedWords
    }
    
    static func kebabCase(_ words: [String]) -> [String]
    {
        var convertedWords = words
        convertedWords = [convertedWords.joined(separator: "-")]
        return convertedWords
    }
    
    static func upperCase(_ words: [String]) -> [String]
    {
        return words.map{
            $0.uppercased()
        }
    }
    
    static func titleCase(_ words: [String]) -> [String]
    {
        return words.map{
            $0.capitalized
        }
    }
    
    static func spongeCase(_ words: [String]) -> [String]
    {
        let charArray = words.joined(separator: "").enumerated()
        let convertedWords = charArray.map()
        {
            $0.offset % 2 == 0 ? String($0.element).uppercased() : String($0.element)
        }.joined(separator: "")
        return [convertedWords]
    }
    
    static func stringCase(_ words: [String]) -> [String]
    {
        let convertedWords = "\"" + words.joined(separator: " ") + "\""
        return [convertedWords]
    }
}
