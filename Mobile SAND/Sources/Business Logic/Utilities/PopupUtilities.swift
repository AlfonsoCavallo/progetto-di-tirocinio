//
//  PopupUtilities.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 23/06/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

//Mostra un popup che esegue un'azione
func showPopup(presenter: MainViewController, popupText: String, actionText: String, cancelText: String, action: (() -> Void)?)
{
    func executableAction(_: UIAlertAction)
    {
        if action != nil { action!() }
    }
    
    //Crea l'alert per l'inserimento del nome
    let alertController = UIAlertController(title: popupText, message: nil, preferredStyle: .alert)
    
    //Crea le azioni e le assegna
    let saveAction = UIAlertAction(title: actionText, style: .default, handler: executableAction)
    let cancelAction = UIAlertAction(title: cancelText, style: .cancel, handler: nil)
    alertController.addAction(saveAction)
    alertController.addAction(cancelAction)
    presenter.present(alertController, animated: true)
}

//Mostra un popup che esegue un'azione acquisendo come parametro una stringa
func showInsertPopup(presenter: MainViewController, popupText: String, actionText: String, cancelText: String, action: ((String?) -> Void)?)
{
    func executableAction(alertAction: UIAlertAction)
    {
        if action != nil {action!(alertController.textFields?.first?.text)}
    }
    //Crea l'alert per l'inserimento del nome
    let alertController = UIAlertController(title: popupText, message: nil, preferredStyle: .alert)
    alertController.addTextField(configurationHandler: .none)
    
    //Crea le azioni e le assegna
    let saveAction = UIAlertAction(title: actionText, style: .default, handler: executableAction)
    let cancelAction = UIAlertAction(title: cancelText, style: .cancel, handler: nil)
    alertController.addAction(saveAction)
    alertController.addAction(cancelAction)
    presenter.present(alertController, animated: true)
}
