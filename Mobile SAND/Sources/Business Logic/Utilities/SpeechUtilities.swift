//
//  SpeechUtilities.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 25/04/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import Speech

//Pronuncia tramite sintetizzatore la string inserita
func syntetize(_ string: String)
{
    let speechSynthetizer = AVSpeechSynthesizer() //Costruisce il sintetizzatore
    let speechUtterance = AVSpeechUtterance(string: string) //Configura la frase da pronunciare
    speechUtterance.rate = AVSpeechUtteranceDefaultSpeechRate
    speechUtterance.voice = AVSpeechSynthesisVoice(identifier: NSLocale.preferredLanguages[0])
    speechSynthetizer.speak(speechUtterance)
    let _ = try? AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
}

