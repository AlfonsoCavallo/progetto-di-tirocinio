//
//  File.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 26/04/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import Foundation

//Comode funzioni di conversione

func stringToWords(_ string: String) -> [String]
{
    return string.lowercased().components(separatedBy: " ")
}

func wordsToString(_ words: [String]) -> String
{
    return String(words.joined(separator: " "))
}

//Introduce lo spazio tra i caratteri speciali
func introduceWhiteBetweenSpecialChar(_ str: String) -> String
{
    let output = str.replacingOccurrences(of: "\n", with: " \n ").replacingOccurrences(of: ",", with: " , ").replacingOccurrences(of: ")", with: " )").replacingOccurrences(of: "(", with: "( ").replacingOccurrences(of: ".", with: " . ").replacingOccurrences(of: ": ", with: " : ").replacingOccurrences(of: "_", with: " _ ").replacingOccurrences(of: "[", with: "[ ").replacingOccurrences(of: "{", with: "{ ")
    return output
}

//Rimuove lo spazio tra i caratteri speciali
func removeWhiteBetweenSpecialChar(_ str: String) -> String
{
    let output = str.replacingOccurrences(of: " \n ", with: "\n").replacingOccurrences(of: " , ", with: ", ").replacingOccurrences(of: " ( ", with: "(").replacingOccurrences(of: " )", with: ")").replacingOccurrences(of: " . ", with: ".").replacingOccurrences(of: " : ", with: ": ").replacingOccurrences(of: "[ ", with: "[").replacingOccurrences(of: " ]", with: "]")
    return output
}

//Trasforma una stringa in un array di stringhe separate da carattere di ritorno
func textToLines(_ text: String) -> [String]
{
    //Crea un array a partire dalla stringa avendo cura di isolare i caratteri di ritorno
    let array = text.components(separatedBy: "\n")
    var output = [String]()
    for elem in array
    {
        output.append(elem)
    }
    return output;
}

//Trasforma un'array di stringhe in una stringa separata da spazi
func linesToText(_ lines : [String]) -> String
{
    return lines.joined(separator: "\n")
}

//Verifica se una stringa è un intero
func isInt(_ str : String) -> Bool
{
    return Int(str) != nil
}
