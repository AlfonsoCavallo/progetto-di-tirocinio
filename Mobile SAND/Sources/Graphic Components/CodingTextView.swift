//
//  CodingTextView.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 11/04/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class CodingTextView: NSObject {

    // MARK: Variabili
    let defaultFontSize = CGFloat(15) //Il valore di default della dimensione del font
    let defaultCodingFont = UIFont(name: "Menlo", size: 15)
    var textView : UITextView //La textView a cui è collegata
    var currentTextCode = "" //Il testo memorizzato nel componente
    var infoText = ""
    var isShowingInfo = false //Stabilisce se la text view sta mostrando info
    {
        didSet
        {
            if isShowingInfo //Se sta mostrando info memorizza il testo
            {
                currentTextCode = text
                alignedText = infoText
            }
            else //Altrimenti torna a mostrare il testo
            {
                text = currentTextCode
            }
        }
    }
    var lineCursor = 1 //La linea selezionata
    {
        didSet
        {
            alignedText = alignedText(lines)
        }
    }
    var text : String //Il testo argomento della textView
    {
        set
        {
            lines = textToLines(newValue)
        }
        get
        {
            linesToText(lines)
        }
    }
    
    var lines = [String]() //Il testo ordinato in righe
    {
        didSet
        {
            alignedText = alignedText(lines)
        }
    }
    
    var alignedText = "" //Il testo allineato con i numeri di riga
    {
        didSet
        {
            textView.text = alignedText
        }
    }
    
    //Stabilisce se è possibile modificare la textView
    var isEditable : Bool
    {
        get
        {
            textView.isEditable
        }
        set
        {
            textView.isEditable = newValue
        }
    }
    
    init(_ textView: UITextView!)
    {
        self.textView = textView
    }
    
    //MARK: Metodi
    //Restituisce il testo allineato con il numero di riga come intestazione
    private func alignedText(_ lines: [String]) -> String
    {
        //Conta le cifre di un intero
        func numberOfDigits(_ int: Int) -> Int
        {
            return String(int).compactMap{ $0.wholeNumberValue }.count
        }
        
        //Restituisce la stringa che rappresenta il numero di riga
        func lineNumberString(_ lineNumber: Int) -> String
        {
            let lineNumberSpace = 4 //Lo spazio che occupa il numero di riga
            let indentationSpace = 2 //Lo spazio occupato dopo il numero di riga
            var output = String()
            if numberOfDigits(lineNumber) < lineNumberSpace
            {
                for _ in 0...(lineNumberSpace - numberOfDigits(lineNumber))
                {
                    if lineNumber == lineCursor
                    {
                        output.append(">") //Aggiunge cursori finchè possibile
                    }
                    else
                    {
                        output.append(" ") //Aggiunge spazi bianchi finchè possibile
                    }
                }
                output.append(String(lineNumber)) //Aggiunge alla fine il numero di riga
            }
            else
            {
                for _ in 0...lineNumberSpace
                {
                    output.append(".") //Aggiunge spazi bianchi finchè possibile
                }
            }
            for _ in 0...indentationSpace
            {
                output.append(" ") //Aggiunge spazi bianchi dopo il numero di riga
            }
            return output
        }
        var output = String()
        for index in 0..<lines.count
        {
            output.append(lineNumberString(index + 1)) //Antepone il numero di riga
            output.append(lines[index]) //Inserisce la riga
            output.append("\n") //Va a capo
        }
        
        return output
    }
    
    //Porta il cursore alla fine se eccede il conto delle righe
    func adjustCursor()
    {
        if lineCursor > lines.count
        {
            lineCursor = lines.count
        }
    }
    
    //Mostra le info
    func triggerInfo(infoText : String)
    {
        if !isShowingInfo
        {
            self.infoText = infoText //Memorizza il testo delle informazioni
            textView.font = UIFont.systemFont(ofSize: defaultFontSize) //Modifica il font
            isShowingInfo = true //Abilita la lettura delle informazioni
        }
        else
        {
            textView.font = defaultCodingFont //Modifica il font
            isShowingInfo = false
        }
    }
}
