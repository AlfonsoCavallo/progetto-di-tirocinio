//
//  TableViewCell.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 23/06/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class ScriptViewCell: UITableViewCell {

    
    @IBOutlet weak var scriptName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
