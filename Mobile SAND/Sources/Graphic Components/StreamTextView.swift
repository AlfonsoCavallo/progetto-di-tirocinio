//
//  StreamTextView.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 11/05/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class StreamTextView: NSObject {

    // MARK: Variabili
    let defaultFontSize = CGFloat(18) //Il valore di default della dimensione del font
    let defaultOutputFont = UIFont(name: "Menlo", size: 15)
    var textView : UITextView //La textView a cui è collegata
    var currentTextCode = "" //Il testo memorizzato nel componente
    var infoText = ""
    var isShowingInfo = false //Stabilisce se la text view sta mostrando info
    {
        didSet
        {
            if isShowingInfo //Se sta mostrando info memorizza il testo
            {
                currentTextCode = textView.text
                textView.text = infoText
            }
            else //Altrimenti torna a mostrare il testo
            {
                textView.text = currentTextCode
            }
        }
    }
    var text : String //Shortcut per il testo della textView
    {
        set
        {
            textView.text = newValue
        }
        get
        {
            textView.text
        }
    }
    
    init(_ textView: UITextView!)
    {
        self.textView = textView
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UIColor.systemGray.cgColor
    }
    
    //Mostra le info
    func triggerInfo(infoText : String)
    {
        if !isShowingInfo
        {
            self.infoText = infoText //Memorizza il testo delle informazioni
            textView.font = UIFont.systemFont(ofSize: defaultFontSize) //Modifica il font
            isShowingInfo = true //Abilita la lettura delle informazioni
        }
        else
        {
            textView.font = defaultOutputFont //Modifica il font
            isShowingInfo = false
        }
    }
}
