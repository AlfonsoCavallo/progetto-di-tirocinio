//
//  ViewController.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 22/03/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

class MainViewController: UIViewController
    {
    // MARK: Outlets dei componenti
    
    //Navigation Bar
    @IBOutlet weak var navbar: UINavigationItem!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var infoButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var organizeButton: UIBarButtonItem!
    
    //Toolbar
    @IBOutlet weak var micButton: UIBarButtonItem!
    
    //Others
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var secondaryTextView: UITextView!
    var codingTextView: CodingTextView?
    var streamTextView: StreamTextView?
    
    //Risorse
    var dictionaryChecker: DictionaryChecker?
    
    //Enum
    enum MicButtonStatus {
        case notListening
        case listening
        case disabled
    }
    
    // MARK: Modalità di funzionamento
    var isShowingInfo = false
    
    // MARK: Variabili di conversione
    let lang = NSLocale.preferredLanguages[0]
    var previousLines = [String]()
    var isConverting = false

    // MARK: Variabili per lo speech to text
    var speechRecognizer : SFSpeechRecognizer?
    var speechRecognitionRequest : SFSpeechAudioBufferRecognitionRequest?
    var speechRecognitionTask : SFSpeechRecognitionTask?
    
    // MARK: Variabili per l'audio
    var isPlaying = false
    var audioSession : AVAudioSession!
    var audioEngine = AVAudioEngine()
    var firstOpen = true

    // MARK: Delegati
    var audioDelegate: AudioDelegate?
    var codingTextViewDelegate: CodingTextViewDelegate?
    
    // MARK: Variabili per il file system
    let defaultScriptName = "new_script" //Il nome di default di un nuovo script
    var scriptName: String? //Il nome dello script attualmente in uso
    {
        didSet
        {
            navbar.title = scriptName ?? defaultScriptName
        }
    }
    let fileManager = FileManager.default
    var documentsURL : URL?
    
    // MARK: ViewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //Inizializza il titolo e le textView
        navbar.title = defaultScriptName
        //Prepara il dictionaryChecker
        self.dictionaryChecker = try? DictionaryChecker(lang)
        //Prepara lo speechDelegate
        self.audioDelegate = AudioDelegate(vc: self)
        //Prepara il codingTextViewDelegate
        self.codingTextViewDelegate = CodingTextViewDelegate(vc: self)
        //Prepara il riconoscitore
        speechRecognizer?.delegate = audioDelegate
        speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: lang))
        //Costruisce la coding textView
        textView.delegate = self.codingTextViewDelegate
        self.codingTextView = CodingTextView(textView)
        codingTextView!.text = ""
        codingTextView!.lineCursor = 0
        //Costruisce la stream textView
        self.streamTextView = StreamTextView(secondaryTextView)
        streamTextView!.text = ""
        addDoneButtonOnKeyboard()
        //Inizializza le classi per la gestione dei files
        documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    // MARK: Funzioni per il registratore
    
    //Avvia la procedura di registrazione
    func startRecording()
    {
        //Verifica se ci sono tasks in corso
        if speechRecognitionTask != nil{
            speechRecognitionTask?.cancel()
            speechRecognitionTask = nil
        }
        
        //Prepara la sessione audio
        audioSession = AVAudioSession.sharedInstance()
        
        do
        {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true)
        }
        catch
        {
            streamTextView?.text = "Could not start audio session"
        }
        
        //Prepara la richiesta
        speechRecognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        //Prepara il riconoscitore
        let inputNode = audioEngine.inputNode
        
        guard let speechRecognitionRequest = speechRecognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        speechRecognitionRequest.shouldReportPartialResults = true
        
        let converter : CodeConverter
        let interpreter : CommandInterpreter
        
        //Prova ad inizializzare il convertitore e l'interprete
        do
        {
            converter = try CodeConverter(vc: self)
            interpreter = try CommandInterpreter(vc: self)
        }
        catch let error
        {
            print("ERROR: Failed to initialize Interpreter or Converter - ", error)
            closeAudioSession()
            return
        }
        
        //Assegna il task
        speechRecognitionTask = speechRecognizer?.recognitionTask(with: speechRecognitionRequest)
        {
            (result, error) in
            
            var isFinal = false //isFinal è inizialmelmente false
            let transcription = result?.bestTranscription.formattedString //Memorizza la trascrizione
            
            //Esegue tutte le operazioni di interpretazione del risultato in tempo reale
            if result != nil
            {
                let textToAnalyze = interpreter.execute(transcription ?? "")
                if textToAnalyze == "CloseAudioSession"
                {
                    self.closeAudioSession()
                }
                else if textToAnalyze != nil
                {
                    //Conserva lo stato delle righe della codingTextView solo se è appena iniziata la fase di conversione
                    if !self.isConverting
                    {
                        self.previousLines = [String](self.codingTextView!.lines)
                        self.isConverting = true
                    }
                    //Conserva il testo restituito dall'interprete come code
                    let conversionResult = converter.convert(textToAnalyze!)
                    let code = conversionResult.convertedString
                    self.codingTextView!.lines = [String](self.previousLines) //Ripristina le righe allo stato precendete
                    if self.codingTextView!.lineCursor < self.codingTextView!.lines.count //Se il cursore non punta all'ultimo elemento effettua una insert
                    {
                        self.codingTextView!.lines.insert(contentsOf: textToLines(code), at: self.codingTextView!.lineCursor) //Inserisce il testo nella giusta posizione
                    }
                    else //Altrimento accoda la nuova riga all'ultimo elemento
                    {
                        self.codingTextView!.lines.append(contentsOf: textToLines(code))
                    }
                    if conversionResult.closeAudioSession //Interrompe la sessione audio se individua il comando di end
                    {
                        self.closeAudioSession()
                    }
                }
                isFinal = (result?.isFinal)!
            }
            
            //Interrompe la registrazione in caso di errore o di tempo scaduto
            if error != nil || isFinal
            {
                self.closeAudioSession() //Interrompe la sessione audio
            }
        }
        
        //Prepara il nodo di input per la registrazione
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.speechRecognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare() //Prepara l'audio engine
    
        do
        {
            try audioEngine.start() //Prova ad avviare l'audio engine
        }
        catch
        {
            codingTextView!.isEditable = true //L'area di testo è di nuovo modificabile
            AudioServicesPlaySystemSound(1521) //Vibrazione che avvisa di un malfunzionamento
        }
        
        AudioServicesPlayAlertSound(1432) //Notifica l'avvia della registrazione
        //Avvisa che si è in ascolto modificando le grafiche del bottone
        setMicButtonGraphic(.listening)
    }
    
    // MARK: Interazioni coi bottoni
    
    //Azione del buttone del microfono
    @IBAction func micButtonAction(_ sender: Any)
    {
        if showingButtonInfo(.mic) { return }
        
        var canRecord = true
        
        //Richiesta per i permessi di registrare
        AVAudioSession.sharedInstance().requestRecordPermission{ (hasPermission) in
            if !hasPermission
            {
                canRecord = false
            }
        }
        
        //Richiesta per i permessi di utilizzare le funzionalità speechToText
        SFSpeechRecognizer.requestAuthorization { (hasPermission) in
            switch hasPermission
            {
            case .notDetermined:
                canRecord = false
                print("Cannot work because of not determined status of request")
            case .denied:
                canRecord = false
                print("Cannot work because request has been refused")
            case .restricted:
                canRecord = false
                print("Cannot work because of restriction on speech request")
            case .authorized:
                print("Request accepted")
            @unknown default:
                print("Cannot work because this status cannot be handled")
                canRecord = false
            }
        }
        
        //Verifica nuovamente lo status
        if SFSpeechRecognizer.authorizationStatus() != .authorized
        {
            canRecord = false
        }
        
        //Verifica se l'engine audio è già in funzione
        if canRecord
        {
            setMicButtonGraphic(.notListening)
            if audioEngine.isRunning
            {
                //Procedura che interrompe la registrazione
                audioEngine.stop()
                audioEngine.inputNode.removeTap(onBus: 0)
                speechRecognitionTask?.cancel()
                speechRecognitionTask?.finish()
                speechRecognitionRequest?.endAudio()
                codingTextView!.isEditable = true
            }
            else
            {
                codingTextView!.isEditable = false
                startRecording()
            }
        }
        else
        {
            if firstOpen
            {
                firstOpen = false
            }
            else
            {
                setMicButtonGraphic(.disabled) //In qualsiasi caso in cui il microfono non può registrare viene modificata la sua grafica
            }
        }
    }
    
    //Apre la cartella degli script
    @IBAction func organizeButtonAction(_ sender: Any) {
        if showingButtonInfo(.organize) { return }
    }
    
    //Salva lo script
    @IBAction func saveButtonAction(_ sender: Any) {
        if showingButtonInfo(.save) { return }
        saveAttempt()
    }
    
    //Crea un nuovo script
    @IBAction func addButtonAction(_ sender: Any) {
        if showingButtonInfo(.add) { return }
        createNewScript()
    }
    
    @IBAction func infoButtonAction(_ sender: Any)
    {
        var infoText = ""
        do
        {
            closeAudioSession() //Chiude sessione audio e avvio la modalità info
            isShowingInfo = !isShowingInfo
            
            if !isShowingInfo //Ripristina la modalità normale se isShowingInfo è false
            {
                infoButton.image = UIImage(systemName: "info.circle")
                codingTextView?.triggerInfo(infoText: "")
                streamTextView?.triggerInfo(infoText: "")
                return
            }
            
            infoButton.image = UIImage(systemName: "info.circle.fill")
            codingTextView?.textView.isEditable = false //Impedisce la modifica della codingTextView
            //Riempie il testo delle informazioni
            //Comandi
            infoText += self.dictionaryChecker!.informations[DictionaryChecker.InfoEnum.commands.rawValue]![0] + "\n"
            infoText += "\n"
            for commandKey in self.dictionaryChecker!.commandsInfo.keys.sorted()
            {
                infoText += commandKey + ": " + self.dictionaryChecker!.commandsInfo[commandKey]![0] + "\n"
            }
            infoText += "\n"
            //Statements
            infoText += self.dictionaryChecker!.informations[DictionaryChecker.InfoEnum.statements.rawValue]![0] + "\n"
            infoText += "\n"
            for statementKey in self.dictionaryChecker!.statementsInfo.keys.sorted()
            {
                infoText += statementKey + ": " + self.dictionaryChecker!.statementsInfo[statementKey]![0] + "\n"
            }
            infoText += "\n"
            //Notations
            infoText += self.dictionaryChecker!.informations[DictionaryChecker.InfoEnum.notations.rawValue]![0] + "\n"
            infoText += "\n"
            for notationKey in self.dictionaryChecker!.notationsDictionary.keys.sorted()
            {
                infoText += notationKey + "\n"
            }
            infoText += "\n"
            //Keywords
            infoText += self.dictionaryChecker!.informations[DictionaryChecker.InfoEnum.keywords.rawValue]![0] + "\n"
            infoText += "\n"
            for keywordKey in self.dictionaryChecker!.keywordsDictionary.keys.sorted()
            {
                infoText += keywordKey + "   >>>   "
                for keyword in self.dictionaryChecker!.keywordsDictionary[keywordKey]!
                {
                    infoText += keyword + ", "
                }
                infoText += "\n"
            }
            codingTextView!.triggerInfo(infoText: infoText)
            streamTextView!.triggerInfo(infoText: dictionaryChecker!.buttons["Intro"]?.first ?? "")
        }
        catch
        {
            codingTextView!.triggerInfo(infoText: "Failed to load files")
        }
    }
    
    //Esegue l'azione run
    @IBAction func runButtonAction(_ sender: Any)
    {
        
        run()
    }
    
    // MARK: Metodi relativi alle azioni
    
    //Verifica che il nome dello script sia stato definito
    func saveAttempt()
    {
        //Salva lo script dopo averne assegnato il nome
        func scriptSave(text : String?)
        {
            scriptName = text ?? "new_script"
            save()
        }
        
        //Se il nome dello script è assegnato effettua il salvataggio
        if scriptName != nil
        {
            save()
            return
        }
        
        //crea un popup per salvare il file ed inserire il nome del file
        showInsertPopup(presenter: self, popupText: "Enter a script name", actionText: "Save", cancelText: "Cancel", action: scriptSave)
    }
    
    //Salva il file con il nome dello script in formato .swift
    func save()
    {
        if documentsURL != nil
        {
            //Costruisce l'URL di salvataggio
            let saveURL = documentsURL!.appendingPathComponent(scriptName!).appendingPathExtension("swift")
            do
            {
                //Prova a a salvare il file
                try fileSave(URL: saveURL, content: codingTextView?.text ?? "")
                streamTextView?.text = "Script correctly saved"
            }
            catch
            {
                streamTextView?.text = "Could not save the script"
            }
        }
        else
        {
            streamTextView?.text = "No document directory available"
        }
    }
    
    //Prova ad aprire il file
    func open(_ URL: URL)
    {
        do
        {
            let script = try fileOpen(URL)
            scriptName = URL.deletingPathExtension().lastPathComponent
            codingTextView?.text = script
            streamTextView?.text = "Script succesfully opened"
        }
        catch
        {
            streamTextView?.text = "Cannot open the script"
        }
    }
    
    //Crea un nuovo file
    func createNewScript()
    {
        //Crea un nuovo script
        func createNewScript()
        {
            //Cancella tutto il testo
            scriptName = nil
            codingTextView!.text = ""
            streamTextView!.textView.text = ""
            codingTextView!.lineCursor = 0
        }
        //Mostra un popup di conferma
        showPopup(presenter: self, popupText: "Unsaved modifies will be lost. Are you sure you want to create a new script?", actionText: "Create", cancelText: "Cancel", action: createNewScript)
    }
    
    //Esegue il codice
    func run()
    {
        if showingButtonInfo(.run) { return }
        CompilerInterface.init().execute(codeFragment: codingTextView!.text, in: streamTextView!)
    }
    
    
    // MARK: Altre funzioni utili
    
    //Mostra le info del bottone se la modalità attiva è quella delle info
    func showingButtonInfo(_ button : DictionaryChecker.ButtonEnum) -> Bool
    {
        if isShowingInfo
        {
            secondaryTextView!.text = self.dictionaryChecker!.buttons[button.rawValue]?.first ?? ""
            return true
        }
        else
        {
            return false
        }
    }
    
    //Interrompe la sessione di registrazione
    func closeAudioSession()
    {
        //Spegne la sessione di registrazione
        func stopEngine()
        {
            //Procedura che interrompe la registrazione
            self.audioEngine.stop()
            self.audioEngine.inputNode.removeTap(onBus: 0)
            self.speechRecognitionTask?.cancel()
            self.speechRecognitionTask?.finish()
            self.speechRecognitionRequest?.endAudio()
            self.codingTextView!.isEditable = true
        }
        //Se è stato aggiunto nuovo codice e c'è stata una conversione, sposta in avanti il cursore di posizioni pari al numero di righe scritte
        if previousLines.count < codingTextView!.lines.count && isConverting
        {
            codingTextView!.lineCursor += codingTextView!.lines.count - previousLines.count
            codingTextView!.adjustCursor()
        }
        isConverting = false //Interrompe la conversione
        setMicButtonGraphic(.notListening) //Avvisa che il microfono non sta più registrando modificandone la grafica
        stopEngine()
    }
    
    // MARK: Blocco per aggiungere il pulsante "Done" alla tastiera
    func addDoneButtonOnKeyboard() {
        //Inizializza la toolBar
        let doneToolbar = UIToolbar()
        doneToolbar.sizeToFit()
        //Crea il layout
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        //Crea il pulsante
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: nil, action: #selector(self.doneButtonAction))
        
        //Imposta il tutto
        doneToolbar.setItems([flexibleSpace, doneButton], animated: false)

        self.textView.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction() {
        self.textView.resignFirstResponder()
    }
    
    // MARK: Funzioni grafiche
    func setMicButtonGraphic(_ status: MicButtonStatus)
    {
        switch status
        {
        case .notListening:
            micButton.image = UIImage(systemName: "mic")
            micButton.tintColor = UIColor.red
        case .listening:
            micButton.image = UIImage(systemName: "waveform")
            micButton.tintColor = UIColor.green
        case .disabled:
            micButton.image = UIImage(systemName: "mic.slash")
            micButton.tintColor = UIColor.darkGray
        }
    }
    
    // MARK: Funzioni segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ScriptViewController"
        {
            //ViewController prepara il tableViewController passandogli il proprio reference
            if let tableViewController = segue.destination as? ScriptViewController
            {
                tableViewController.vc = self
            }
        }
    }
}

