//
//  TableViewController.swift
//  Progetto di tirocinio
//
//  Created by Alfonso Cavallo on 23/06/2020.
//  Copyright © 2020 Alfonso Cavallo. All rights reserved.
//

import UIKit

class ScriptViewController: UITableViewController {

    @IBOutlet var scriptView: UITableView!
    var vc : MainViewController? //reference al ViewController principale
    var URLs : [URL]? //Array di URLs degli script
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scriptView.delegate = self
        scriptView.dataSource = self
        //Inizializza l'array di URL
        do {
            self.URLs = try vc!.fileManager.contentsOfDirectory(at: vc!.documentsURL!, includingPropertiesForKeys: nil)
        } catch {
            print("Error in file loading")
        }
    }
    
    // MARK: Metodi da tableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        vc!.open(URLs![indexPath.row])
        vc!.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete
        {
            do
            {
                try vc!.fileManager.removeItem(at: URLs![indexPath.row])
                URLs!.remove(at: indexPath.row)
                
                tableView.beginUpdates()
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
                tableView.endUpdates()
            }
            catch
            {
                
            }
        }
    }
    
    // MARK: Metodi di DataSource
    //Numero di sezione
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //Numero di celle mostrate per ogni sezione
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return URLs?.count ?? 0
    }
    
    //Crea le celle e le riporta nelle tabella in ordine
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ScriptViewCell
        cell.scriptName.text = URLs![indexPath.row].lastPathComponent //Assegna il testo ai tasti
        return cell
    }
}
