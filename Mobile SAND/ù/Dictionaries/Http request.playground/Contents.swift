import PlaygroundSupport
import NaturalLanguage
PlaygroundPage.current.needsIndefiniteExecution = true

extension String {
    func toHexEncodedString(uppercase: Bool = true, prefix: String = "", separator: String = "%") -> String {
        return unicodeScalars.map { prefix + .init($0.value, radix: 16, uppercase: uppercase) } .joined(separator: separator)
    }
}


//HTTP Methods
enum HttpMethod : String {
   case  GET
   case  POST
   case  DELETE
   case  PUT
}

class HttpClientApi: NSObject{
//TODO: remove app transport security arbitary constant from info.plist file once we get API's
 var request : URLRequest?
 var session : URLSession?

static func instance() ->  HttpClientApi{

    return HttpClientApi()
}

func makeAPICall(url: String,params: Dictionary<String, Any>?, method: HttpMethod, success:@escaping ( Data? ,HTTPURLResponse?  , NSError? ) -> Void, failure: @escaping ( Data? ,HTTPURLResponse?  , NSError? )-> Void) {

    request = URLRequest(url: URL(string: url)!)

    print("URL = \(url)")

    if let params = params {


        let  jsonData = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)

        request?.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request?.httpBody = jsonData//?.base64EncodedData()


        //paramString.data(using: String.Encoding.utf8)
    }
    request?.httpMethod = method.rawValue


    let configuration = URLSessionConfiguration.default

    configuration.timeoutIntervalForRequest = 30
    configuration.timeoutIntervalForResource = 30

    session = URLSession(configuration: configuration)
    //session?.configuration.timeoutIntervalForResource = 5
    //session?.configuration.timeoutIntervalForRequest = 5

    session?.dataTask(with: request! as URLRequest) { (data, response, error) -> Void in

        if let data = data {

            if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                success(data , response , error as NSError?)
            } else {
                failure(data , response as? HTTPURLResponse, error as NSError?)
            }
        }else {

            failure(data , response as? HTTPURLResponse, error as NSError?)

        }
        }.resume()

  }

}


var paramsDictionary = ["source_code": "a = 1\nprint(a)", "language": "swift", "api_key" : "guest"]
var id = ""
var done = false

struct Body: Decodable
{
    var id: String
    var status: String
}

struct Body2: Decodable
{
    var build_stdout: String
}

let client = HttpClientApi.instance()

client.makeAPICall(url: "http://api.paiza.io/runners/create", params:paramsDictionary, method: .POST, success: { (data, response, error) in

    print(response!.statusCode)
    print(try! JSONDecoder.init().decode(Body.self, from: data!))
    id = try! JSONDecoder.init().decode(Body.self, from: data!).id
    done = true
    
}, failure: { (data, response, error) in

    print("fail")

})

sleep(5)

print("http://api.paiza.io:80/runners/get_details?id=" + id + "&api_key=guest")
client.makeAPICall(url: "http://api.paiza.io/runners/get_details" + "?id=" + id + "&api_key=guest", params: nil, method: .GET, success: { (data, response, error) in

    print(response!.statusCode)
    print(try! JSONDecoder.init().decode(Body2.self, from: data!))
    print(data!)


}, failure: { (data, response, error) in

    print(data, response, error)

})

