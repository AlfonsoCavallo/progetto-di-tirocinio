line - say this following by the number of line you want the cursor to reach
read - read the current line
run - build and execute the code
cut - cut the line
copy - copy the line
paste - paste the line
delete - delete the line
write - start coding
stop - stop navigating the IDE
search - search for a word in the code
new - create a new file
rename - rename the file (say ok or stop to rename)
save - save the file if it's already named
open - open the file (say ok or stop to open)
run - executes the code and shows output on the stream
